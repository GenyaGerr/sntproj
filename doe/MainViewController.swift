//
//  MainViewController.swift
//  doe
//
//  Created by mac_of_kadjit on 10/06/2019.
//  Copyright © 2019 mac_of_kadjit. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {

    @IBAction func patreonButton(_ sender: Any) {
    }
    
    @IBAction func retoolButton(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.delegate = self
        //self.tableView.dataSource = self        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(indexPath.row + 1)", for: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return UIScreen.main.bounds.height - CGFloat(350)
        }
        return 350
    }

}
